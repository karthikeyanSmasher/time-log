package com.example.sts_269.clockdelete;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class TimerFragment extends Fragment {


    View viewToLoad;

    String status="0";

    private TextView wDays;
    private TextView wHours;
    private TextView wMinutes;
    private TextView wSeconds;
    private TextView bDays;
    private TextView bHours;
    private TextView bMinutes;
    private TextView bSeconds;
    private Button startWork;
    private Button stopWork;
    private Button startBreak;
    private Button stopBreak;
    private Button reset;

    private String preference = "preference_data";

    private String worktime = "worktime";

    private String breaktime = "breaktime";

    private String status_save = "status_save";



    long workTime = 0;
    long breakTime = 0;

    Handler h1 = new Handler();

    Runnable run1 = new Runnable() {

        @Override
        public void run() {

            workTime = workTime +1000;

            long different = workTime;

            if (different < 0) {
                different = different * -1;
            }

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            wDays.setText(elapsedDays + "");
            wHours.setText(elapsedHours + "");
            wMinutes.setText(elapsedMinutes + "");
            wSeconds.setText(elapsedSeconds + "");

            h1.postDelayed(this, 1000);

        }

    };


    Context mContext;

    Timer timer1 = new Timer();
    Timer timer2 = new Timer();


    Handler h2 = new Handler();

    Runnable run2 = new Runnable() {

        @Override
        public void run() {

            breakTime = breakTime +1000;

            long different = breakTime;

            if (different < 0) {
                different = different * -1;
            }

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            bDays.setText(elapsedDays + "");
            bHours.setText(elapsedHours + "");
            bMinutes.setText(elapsedMinutes + "");
            bSeconds.setText(elapsedSeconds + "");

            h2.postDelayed(this, 1000);

        }

    };


    public TimerFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public TimerFragment(Context mContext) {
        // Required empty public constructor
        this.mContext = mContext;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewToLoad = inflater.inflate(R.layout.fragment_timer, container, false);

        init();

        setListeners();

        loadData();

        // Inflate the layout for this fragment
        return viewToLoad;
    }

    private void loadData() {

      loadSavedValues();
    }

    private boolean loadSavedValues() {

        SharedPreferences prefs = mContext.getSharedPreferences(preference, MODE_PRIVATE);

        workTime = prefs.getLong(worktime,0);
        breakTime = prefs.getLong(breaktime,0);
        status = prefs.getString(status_save,"0");

        switch (status){

            case "0":

                reset.performClick();

                break;

            case "1":

                startWork.performClick();


            break;

            case "2":

                stopWork.performClick();

                break;

            case "3":

                startBreak.performClick();

                break;

            case "4":

                stopBreak.performClick();

                break;


        }

        return false;

    }

    private void setListeners() {

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                workTime = 0;

                breakTime = 0;

                wDays.setText("0");
                wHours.setText("0");
                wMinutes.setText("0");
                wSeconds.setText("0");

                bDays.setText("0");
                bHours.setText("0");
                bMinutes.setText("0");
                bSeconds.setText("0");

                startWork.setVisibility(View.VISIBLE);

                reset.setVisibility(View.GONE);

                status="0";

                SharedPreferences.Editor editor = mContext.getSharedPreferences(preference, MODE_PRIVATE).edit();

                editor.clear();

                editor.apply();

            }
        });

        startWork.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                timer1 = new Timer();

                h1.postDelayed(run1, 0);

                startWork.setVisibility(View.GONE);

                stopWork.setVisibility(View.VISIBLE);

                startBreak.setVisibility(View.VISIBLE);

                status="1";

            }
        });

        stopWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                timer1.cancel();

                timer1.purge();

                h1.removeCallbacks(run1);

                startWork.setVisibility(View.GONE);

                reset.setVisibility(View.VISIBLE);

                stopBreak.setVisibility(View.GONE);

                stopWork.setVisibility(View.GONE);

                startBreak.setVisibility(View.GONE);

                status="2";

            }
        });


        startBreak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                timer2 = new Timer();

                h2.postDelayed(run2, 0);

                timer1.cancel();

                timer1.purge();

                h1.removeCallbacks(run1);

                startWork.setVisibility(View.GONE);

                startBreak.setVisibility(View.GONE);

                stopWork.setVisibility(View.GONE);

                stopBreak.setVisibility(View.VISIBLE);

                status="3";



            }
        });

        stopBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                timer1 = new Timer();

                h1.postDelayed(run1, 0);

                timer2.cancel();

                timer2.purge();

                h2.removeCallbacks(run2);

                startWork.setVisibility(View.GONE);

                stopBreak.setVisibility(View.GONE);

                startBreak.setVisibility(View.VISIBLE);

                stopWork.setVisibility(View.VISIBLE);

                status="4";
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        saveValues();

    }

    private void saveValues() {

        SharedPreferences.Editor editor = mContext.getSharedPreferences(preference, MODE_PRIVATE).edit();
        editor.putLong(worktime,workTime );
        editor.putLong(breaktime,breakTime);
        editor.putString(status_save,status);
        editor.apply();
    }


    private void init() {

        wDays = (TextView) viewToLoad.findViewById(R.id.w_days);
        wHours = (TextView) viewToLoad.findViewById(R.id.w_hours);
        wMinutes = (TextView) viewToLoad.findViewById(R.id.w_minutes);
        wSeconds = (TextView) viewToLoad.findViewById(R.id.w_seconds);
        bDays = (TextView) viewToLoad.findViewById(R.id.b_days);
        bHours = (TextView) viewToLoad.findViewById(R.id.b_hours);
        bMinutes = (TextView) viewToLoad.findViewById(R.id.b_minutes);
        bSeconds = (TextView) viewToLoad.findViewById(R.id.b_seconds);
        startWork = (Button) viewToLoad.findViewById(R.id.start_work);
        stopWork = (Button) viewToLoad.findViewById(R.id.stop_work);
        startBreak = (Button) viewToLoad.findViewById(R.id.start_break);
        stopBreak = (Button) viewToLoad.findViewById(R.id.stop_break);
        reset = (Button) viewToLoad.findViewById(R.id.reset);

        wDays.setText("0");
        wHours.setText("0");
        wMinutes.setText("0");
        wSeconds.setText("0");

        bDays.setText("0");
        bHours.setText("0");
        bMinutes.setText("0");
        bSeconds.setText("0");

    }
}
