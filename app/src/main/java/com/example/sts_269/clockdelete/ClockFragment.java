package com.example.sts_269.clockdelete;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClockFragment extends Fragment {


    View viewToLoad;
    Context mContext;

    private TextView wDays;
    private TextView wHours;
    private TextView wMinutes;
    private TextView wSeconds;
    private TextView bDays;
    private TextView bHours;
    private TextView bMinutes;
    private TextView bSeconds;
    private Button startWork;
    private Button stopWork;
    private Button startBreak;
    private Button stopBreak;



    public ClockFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ClockFragment(Context mContext) {

        this.mContext = mContext;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewToLoad =  inflater.inflate(R.layout.fragment_clock, container, false);

        init();

        return viewToLoad;

    }

    private void init() {

        wDays = (TextView) viewToLoad.findViewById(R.id.w_days);
        wHours = (TextView) viewToLoad.findViewById(R.id.w_hours);
        wMinutes = (TextView) viewToLoad.findViewById(R.id.w_minutes);
        wSeconds = (TextView) viewToLoad.findViewById(R.id.w_seconds);
        bDays = (TextView) viewToLoad.findViewById(R.id.b_days);
        bHours = (TextView) viewToLoad.findViewById(R.id.b_hours);
        bMinutes = (TextView) viewToLoad.findViewById(R.id.b_minutes);
        bSeconds = (TextView) viewToLoad.findViewById(R.id.b_seconds);
        startWork = (Button) viewToLoad.findViewById(R.id.start_work);
        stopWork = (Button) viewToLoad.findViewById(R.id.stop_work);
        startBreak = (Button) viewToLoad.findViewById(R.id.start_break);
        stopBreak = (Button) viewToLoad.findViewById(R.id.stop_break);



    }

}
